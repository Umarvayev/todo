<?php


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SaveController;
use App\Http\Controllers\TasksController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\Admin\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');


});
 
Auth::routes();

Route::prefix('admin')->group(function() {

    Route::get('/dashboard', [AdminController::class, 'index']);
    Route::resource('projects', ProjectController::class);

    //tasks

    Route::controller(TasksController::class)->group(function() {
        
        Route::get('/project/{id}/tasks', 'index')->name('main');
        Route::post('/tasks/store', 'store');
        Route::get('/tasks/edit/{id}', 'edit');
        Route::put('/tasks/update/{id}', 'update');
        Route::get('/tasks/destroy/{id}', 'destroy');
        // Route::get('/mytasks', 'mytasks');
        Route::get('/mytasks', 'user_tasks');

    });

    Route::controller(SaveController::class)->group(function() {
   
        Route::put('/save/create/{id}', 'create');

    });

});




Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



