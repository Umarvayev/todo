<hr class="mt-10">

<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
    <!--begin::Container-->
    <div class="container-xxl d-flex flex-column flex-md-row align-items-center justify-content-between">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-gray-700 fw-bold me-1">2022© To Do</span>
            {{-- <a href="{{ route('home') }}"  class="text-gray-800 text-hover-primary">Register Now</a> --}}
        </div>
        
        <ul class="menu menu-gray-700 menu-hover-primary fw-bold order-1">
            <li class="menu-item ">
                <a href="{{ route('projects.index') }}"  class="menu-link px-2">Projects</a>
            </li>
            <li class="menu-item ">
                <a href="{{ url('admin/mytasks') }}" class="menu-link px-2">Mening Vazifalarim</a>
            </li>
        </ul>
    </div>
    <!--end::Container-->
</div>