@extends('layouts.admin')
@section('content')

<div class="col-md-12">

    <div class="row justify-content-center">

        <div class="col-md-7">

            <div class="card">

                <div class="card-header">
                    <div class="m-auto">
                        <h1>Loyihani Tahrirlash</h1>
                    </div>
                </div>
        
                <div class="card-body">
        
        
                    <form action="{{ route('projects.update',$project->id) }}" method="Post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <input type="text" name="name" class="form-control" placeholder="Loyiha Nomi" value="{{ $project->name }}">
                
                        <textarea name="description" id="" cols="30" rows="3" class="form-control my-3" placeholder="Loyiha Tavsifi">{{ $project->description }}</textarea> 
                
                        <input type="file" name="file" class="form-control my-3">
                        <button class="btn btn-primary">Save</button>
                
                    </form>
        
                </div>
        
            </div>

        </div>

    </div>

</div>

@endsection
