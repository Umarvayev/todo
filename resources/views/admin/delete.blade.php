@extends('layouts.admin')

<div class="modal-body py-lg-10 px-lg-10">

    <form action="{{ route('projects.destroy',$project->id) }} " method="POST" enctype="multipart/form-data">
        @csrf

        <input type="text" name="name" class="form-control" placeholder="Name">

        <textarea name="description" id="" cols="30" rows="3" class="form-control my-3" placeholder="Description"></textarea>
        {{-- <textarea name="status" id="" cols="30" rows="3" class="form-control my-3" placeholder="Status"></textarea> --}}

        <input type="file" name="file" class="form-control my-3">

    <form action="{{route('projects.destroy',$project->id)}}" method="Post">
        @method('DELETE')
        <button type="submit" class="btn btn-danger">Delete</button>
    </form>
    </form>

</div>
