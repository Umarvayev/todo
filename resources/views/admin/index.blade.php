@extends('layouts.admin')
@section('content')
    <div class="toolbar py-5 pt-lg-0" id="kt_toolbar">
        <!--begin::Container-->
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <!--begin::Page title-->
            <div class="page-title d-flex flex-column me-3">
                <!--begin::Title-->
                <h1 class="d-flex text-gray-900 fw-bolder my-1 fs-3">Barcha Loyihalar</h1>

            </div>
            <!--end::Page title-->
            <!--begin::Actions-->
            <div class="d-flex align-items-center py-3 py-md-1">


                <div class="m-2">
                    Foydalanuvchi Ismi: <b class="fs-3 m-2">{{ auth()->user()->name }}</b>
                </div>
                <a href="{{ route('projects.create') }}" class="btn btn btn-dark" data-bs-toggle="modal"
                    data-bs-target="#kt_modal_create_app" id="kt_toolbar_primary_button">Yangi Loyiha</a>
                <!--end::Button-->
                     
                <div class="modal fade" id="kt_modal_create_app" tabindex="-1" aria-hidden="true">
                    <!--begin::Modal dialog-->
                    <div class="modal-dialog modal-dialog-centered mw-900px">
                        <!--begin::Modal content-->
                        <div class="modal-content">
                            <!--begin::Modal header-->
                            <div class="modal-header">
                                <!--begin::Modal title-->
                                <h2>Loyiha Yarating</h2>
                                <!--end::Modal title-->
                                <!--begin::Close-->
                                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                                    <span class="svg-icon svg-icon-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2"
                                                rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                            <rect x="7.41422" y="6" width="16" height="2" rx="1"
                                                transform="rotate(45 7.41422 6)" fill="currentColor" />
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </div>
                                <!--end::Close-->
                            </div>
                            <!--end::Modal header-->
                            <!--begin::Modal body-->
                            @include('admin.create')
                            <!--end::Modal body-->
                        </div>
                        <!--end::Modal content-->
                    </div>
                    <!--end::Modal dialog-->
                </div>
            </div>
            <!--end::Actions-->
        </div>
        <!--end::Container-->
    </div>

    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">

        <div class="content flex-row-fluid" id="kt_content">

            <div class="row g-6 g-xl-9">
                <!--begin::Col-->

                @foreach ($project as $item)
                    <div class="col-md-6 col-xl-4 my-2">

                        {{-- @if (auth()->user()->id == $item->user_id) --}}
                            <a href="/admin/project/{{ $item->id }}/tasks">
                        {{-- @endif --}}
                        
                        <!--begin::Card header-->
                        <div class="card border-hover-primary">
                            <div class="card-header border-0 pt-9">
                                <!--begin::Card Title-->
                                <div class="card-title m-0">
                                    <!--begin::Avatar-->
                                    <div class="symbol symbol-50px w-50px bg-light">
                                        <img src="{{ asset('storage/' . $item->file) }}" alt="image" class="" />
                                    </div>
                                    <!--end::Avatar-->

                                </div>
                            </div>


                            <div class="card-body p-9">


                                <!--begin::Name-->
                                <div class="fs-3 fw-bolder text-dark">{{ $item->name }}</div>

                                <!--end::Name-->
                                <!--begin::Description-->
                                <p class="text-gray-400 fw-bold fs-5 mt-1 mb-7">{{ $item->description }}</p>
                                <!--end::Description-->
                                <!--begin::Info-->
                                <div class="d-flex flex-wrap mb-1">
                                    <!--begin::Due-->
                                    <div
                                        class="border border-gray-300 border-dashed rounded min-w-125px py-4 px-4 me-7 mb-3">
                                        <div class="fs-6 text-gray-800 fw-bolder">{{ $item->created_at }}</div>

                                    </div>
                                    <div>

                                        @if (auth()->user()->id == $item->user_id)
                                            <div class="border border-gray-300 border-dashed rounded p-1">

                                                <div class="d-flex  ">
                                                    <div class="m-2">
                                                        <a href="{{ route('projects.edit', $item->id) }}"
                                                            class="btn btn btn-dark">Tahrirlash</a>

                                                    </div>
                                                    <div class="m-2">

                                                        <form action="{{ route('projects.destroy', $item->id) }}"
                                                            method="Post" enctype="multipart/form-data">
                                                            @csrf
                                                            @method('DELETE')

                                                            <button onclick="return confirm('Are you sere')"
                                                                class="btn btn-danger ">O'chirish</button>
                                                        </form>
                                                    </div>

                                                </div>
                                            </div>
                                        @endif
                                    </div>

                                </div>






                            </div>
                        </div>

                        </a>


                    </div>
                @endforeach

            </div>
        </div>

    </div>
@endsection
