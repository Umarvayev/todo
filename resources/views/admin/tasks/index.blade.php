@extends('layouts.admin')
@section('content')

    <div class="toolbar py-5 pt-lg-0" id="kt_toolbar">
        <div id="kt_toolbar_container" class="container-xxl d-flex flex-stack flex-wrap">
            <div class="page-title d-flex flex-column me-3">
                <div class="m-2">
                    Loyiha nomi: <b class="fs-3 m-2">{{ $project->name }}</b>
                </div>

                <div class="m-2">
                    Foydalanuvchi Nomi: <b class="fs-3 m-2">{{ auth()->user()->name }}</b>
                </div>
            </div>


            <div class="d-flex align-items-center py-3 py-md-1">

                @if (auth()->user()->id == $project->user_id)
                    <a href="{{ url('admin.tasks') }}" class="btn btn btn-dark mx-3" data-bs-toggle="modal"
                        data-bs-target="#kt_modal_create_app" id="kt_toolbar_primary_button">Vazifa qo'shish</a>
                @endif

                {{-- <a href="{{ url('admin/mytasks/' . $project->id) }}" class="btn btn-primary">Mening Vazifalarim</a> --}}

                <div class="modal fade" id="kt_modal_create_app" tabindex="-1" aria-hidden="true">

                    <div class="modal-dialog modal-dialog-centered mw-900px">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2>Loyiha qo'shish</h2>
                                <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                                    <span class="svg-icon svg-icon-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                            viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2"
                                                rx="1" transform="rotate(-45 6 17.3137)" fill="currentColor" />
                                            <rect x="7.41422" y="6" width="16" height="2" rx="1"
                                                transform="rotate(45 7.41422 6)" fill="currentColor" />
                                        </svg>
                                    </span>
                                </div>
                            </div>

                            @include('admin.tasks.create')

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="kt_content_container" class="d-flex flex-column-fluid align-items-start container-xxl">

        <div class="content flex-row-fluid" id="kt_content">
            <div class="row g-6 g-xl-9">

                @if (auth()->user()->id == $project->user_id)
                    @foreach ($task as $item)
                        <div class="col-md-6 col-xl-4 my-2">
                            <div class="card">

                                <div class="card-header  align-items-center">
                                    <h1 class="m-0">{{ $item->users->name }}</h1>
                                </div>

                                <div class="card-body">


                                    <div class="col-md-12">
                                        <h1><b>{{ $item->task }}</b></h1>
                                        <div class="d-flex align-items-center gap-3">
                                            <h4 class="mb-0">Holati: </h4>
                                            <div>
                                                @if ($item->status == 1)
                                                    <span class="badge badge-success d-inline-block">Faol</span>
                                                @else
                                                    <span class="badge badge-danger d-inline-block">Faol emas</span>
                                                @endif

                                            </div>

                                        </div>

                                    </div>
                                    {{-- <div class="col-md-12">

                                        <div class="d-flex align-items-center gap-3">
                                            <h4 class="mb-0">Topshiriq: </h4>
                                            <div>

                                                

                                                @if ($item->save == 1)
                                                    <span class="badge badge-success d-inline-block">Bajarildi</span>
                                                @else
                                                    <span class="badge badge-danger d-inline-block">Bajarilmagan</span>
                                                @endif

                                            </div>

                                        </div>
                                    </div> --}}
                                </div>
                                <div class="card-footer d-flex aligin-items-center justify-content-between">
                                    {{-- <form action="{{ url('admin/save/create/' . $item->id) }}" method="post"
                                        onsubmit="return onsubmitForm(this)">

                                        {{ csrf_field() }}
                                        @method('PUT')
                                        <div>
                                            <input type="checkbox" name="save" checked hidden>
                                            <input type="checkbox" name="role_save" checked hidden>
                                            <button class="btn btn-info">Bajarildi</button>

                                        </div>

                                    </form> --}}

                                    <div class="d-flex aligin-items-center ">

                                        <a href="{{ url('admin/tasks/edit/' . $item->id) }}"
                                            class="btn btn-dark mx-2">Tahrirlash</a>
                                        <a onclick="return confirm('Are you sure')"
                                            href="{{ url('admin/tasks/destroy/' . $item->id) }}"
                                            class="btn btn-danger">O'chirish</a>


                                    </div>


                                </div>

                            </div>

                        </div>
                    @endforeach
                @endif

            </div>

        </div>

    </div>

@endsection
