<div class="modal-body py-lg-10 px-lg-10">

    <form action="/admin/tasks/store" method="POST">

        @csrf

        <select name="user_id" class="form-control">
            @foreach ($users as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
            @endforeach
        </select>

        <input type="hidden" name="project_id" value="{{ $project->id }}">

        <input type="text" name="task" class="form-control my-2" placeholder="Vazifa nomi">
        <div>
            <input type="checkbox" name="status" class="form- my-2">
        </div>

        {{-- <button type="submit" class="btn btn-danger">Back</button> --}}
        <button type="submit" class="btn btn-primary">Saqlash</button>

    </form>

</div>
