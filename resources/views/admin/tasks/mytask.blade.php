@extends('layouts.admin')
@section('content')
<div class="container">
    <div>
        <h1 class="m-4 text-center ">Mening Vazifalarim</h1>
    </div>
    @foreach ($my_tasks as $item)
    
    @if (auth()->user()->id)
        @if (auth()->user()->id == $item->user_id)

            <div class="row justify-content-center">

                <div class="col-md-6 my-3">

                    <div class="card">

                        <div class="card-header  align-items-center">
                            <h1 class="m-0">{{ $item->users->name }}</h1>
                            
                        </div>

                        <div class="card-body">


                            <div class="col-md-12">
                                <h1><b>{{ $item->task }}</b></h1>
                                <div class="d-flex align-items-center gap-3">
                                    <h4 class="mb-0">Holati: </h4>
                                    <div>
                                        @if ($item->status == 1)
                                            <span class="badge badge-success d-inline-block">Faol</span>
                                        @else
                                            <span class="badge badge-danger d-inline-block">Faol emas</span>
                                        @endif

                                    </div>

                                </div>

                            </div>
                            <div class="col-md-12">

                                <div class="d-flex align-items-center gap-3">
                                    <h4 class="mb-0">Topshiriq: </h4>
                                    <div>

                                        {{-- <input type="checkbox" {{ $item->save ? 'checked' : 0 }}> --}}

                                        @if ($item->save == 1)
                                            <span class="badge badge-success d-inline-block">Bajarildi</span>
                                        @else
                                            <span class="badge badge-danger d-inline-block">Bajarilmagan</span>
                                        @endif

                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="card-footer d-flex aligin-items-center justify-content-between">
                            <form action="{{ url('admin/save/create/' . $item->id) }}" method="post"
                                onsubmit="return onsubmitForm(this)">

                                {{ csrf_field() }}
                                @method('PUT')
                                <div>
                                    <input type="checkbox" name="save" checked hidden>
                                    <input type="checkbox" name="role_save" checked hidden>
                                    <button class="btn btn-info">Bajarildi</button>

                                </div>

                            </form>

                        </div>

                    </div>

                </div>

            </div>
        
        @endif
    @endif

    @endforeach
</div>
@endsection
