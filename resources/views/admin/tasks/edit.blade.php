@extends('layouts.admin')
@section('content')
    <div class="col-md-12">

        <div class="row justify-content-center">

            <div class="col-md-7">

                <div class="card">

                    <div class="card-header">
                        <div class="m-auto">
                            <h1>Vazifani Tahrirlash</h1>
                        </div>
                    </div>

                    <div class="card-body">


                        <form action="{{ url('admin/tasks/update/' . $tasksedit->id) }}" method="POST">

                            @csrf
                            @method('PUT')

                            <select name="user_id" class="form-control">
                                @foreach ($users as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>

                            <input type="text" name="task" class="form-control my-2" value="{{ $tasksedit->task }}">
                            <div>
                                <input type="checkbox" name="status" class="form- my-2"
                                    {{ $tasksedit->status ? 'checked' : 0 }}>
                            </div>

                            <button type="submit" class="btn btn-primary">Saqlash</button>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>
@endsection
