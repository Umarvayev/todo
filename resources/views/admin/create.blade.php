<div class="modal-body py-lg-10 px-lg-10">

    <form action="{{ route('projects.store') }} " method="POST" enctype="multipart/form-data">
        @csrf

        <input type="text" name="name" class="form-control" placeholder="Loyiha Nomi">

        <textarea name="description" id="" cols="30" rows="3" class="form-control my-3"
            placeholder="Loyiha Tavsifi"></textarea>
        {{-- <textarea name="status" id="" cols="30" rows="3" class="form-control my-3" placeholder="Status"></textarea> --}}

        <input type="file" name="file" class="form-control my-3">

        <button type="submit" class="btn btn-danger">Ortga</button>
        <button type="submit" class="btn btn-primary">Saqlash</button>

    </form>

</div>
