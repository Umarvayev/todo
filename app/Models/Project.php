<?php

namespace App\Models;

use App\Models\Task;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Project extends Model
{
    use HasFactory;

    protected $fillable=['user_id','tasks_id','name','file','description'];


    public function user()
    {
        return $this->belongsTo(User::class);

    }

    public function tasks()
    {
        return $this->belongsTo(Task::class, 'id');
    }
}
