<?php

namespace App\Http\Controllers;

use App\Models\Save;
use App\Models\Task;
use Illuminate\Http\Request;

class SaveController extends Controller
{
    public function index(Save $id)
    {
        $save=Save::find($id);
        return view('admin.index',compact('save'));
    }
    public function create(Request $request, $id) {
        $save = Task::find($id);
        $request->validate([
            'save' => 'required',
        ]);

        $save->update([
            'save' => $request->save ? 1 : 0,
        ]);

        return response()->back();

    }

}
