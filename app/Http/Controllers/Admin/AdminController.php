<?php

namespace App\Http\Controllers\Admin;

use App\Models\Project;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {

        $project = Project::all();
        return view('admin.index', compact('project'))->with(['project'=>Project::latest()->paginate()]);

    }

}
