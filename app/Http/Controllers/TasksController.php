<?php

namespace App\Http\Controllers;

use App\Models\Save;
use App\Models\Task;
use App\Models\User;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\TasksRequest;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        // $saves = Save::find($id);

        $user = Auth::user();
        $users = User::all();
        $project = Project::find($id);
        $task = Task::where('project_id', $project->id)->get();
        $my_tasks = Task::where('user_id', $user->id)->get();
        return view('admin.tasks.index', compact('project', 'task', 'users','my_tasks'))->with(['task' => Task::latest()->paginate()]);

    }

    public function store(Request $request)
    {
        $task = new Task;

        $task->user_id = $request->user_id;
        $task->project_id = $request->project_id;
        $task->task = $request->task;
        $task->status = $request->status ? 1 : 0;
        $task->save();

        return redirect()->back();

    }
    
    public function edit($id)
    {
        $users = User::all();
        $tasksedit = Task::find($id);
        return view('admin.tasks.edit', compact('tasksedit', 'users'));

    }

    
    public function update(Request $request, $id)
    {
        
        $tasks = Task::find($id);

        $tasks->user_id = $request->user_id;
        $tasks->task = $request->task;
        $tasks->status = $request->status ? 1 : 0;
        $tasks->save();

        return redirect('admin/dashboard');
    }

    public function destroy($id)
    {
        $delete = Task::findOrFail($id);
        $delete->delete();

        return redirect()->back();
    }

    public function user_tasks() {

        $user = Auth::user();
        // $users = User::find($id);
        $my_tasks = Task::where('user_id', $user->id)->get();
        return view('admin.tasks.mytask', compact('my_tasks'))->with(['my_tasks' => Task::latest()->paginate()]);

    }


}
