<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreProjectRequest;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $project = Project::all();
        return view('admin.index', compact('project'));
    }   

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProjectRequest $request )
    {
        if($request->hasFile('file'))
        {
            $name=$request->file('file')->getClientOriginalName();
            $path=$request->file('file')->storeAs('files',$name, 'public');
        }
        $project=Project::create([
          'user_id'=>auth()->user()->id,
          'name'=>$request->name,
          'description'=>$request->description,
          'file'=>$path ?? null,

        ]);
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Project $project)
    {
        
    //   User::all();
    if(auth()->user()->id !== $project->user_id ){

      Project::find($project->id);
  return redirect()->back();
    }
      return view('admin.edit',compact('project'));
    }


  

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Project $project)
    {
        if($request->hasFile('file'))
        
        {
            if(isset($project->photo)){
                Storage::delete($project->file);
            }
            
            $name=$request->file('file')->getClientOriginalName();
            $path=$request->file('file')->storeAs('files',$name, 'public');
        }
        if(auth()->user()->id == $project->user_id ){

        $project->update([
          'user_id'=>auth()->user()->id,
          'name'=>$request->name,
          'description'=>$request->description,
          'file'=>$path ?? $project->file,

        ]);
    }
        return redirect()->route('projects.index',['project'=>$project->id]);
        

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Project $project)
    {
        $project=Project::find($project->id);
        $project->delete();
        
        return redirect()->back();

    }
}

//w